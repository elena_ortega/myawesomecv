package com.CGO.myawesomeCV;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectionDAO extends CrudRepository<Section, Long> {

}
