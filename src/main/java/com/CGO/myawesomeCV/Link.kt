package com.CGO.myawesomeCV

import javax.persistence.*

@Entity(name = "links")
data class Link(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "social_network") var socialNetwork: String?,
        @Column(name = "link_network") var linkNetwork: String?,
        @Column(name = "is_checked") var isChecked: Int?) {
    constructor() : this(null, null, null, 0)
}