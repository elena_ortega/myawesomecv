package com.CGO.myawesomeCV

import java.util.*
import javax.persistence.*


@Entity(name = "sections")
data class Section(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "section_name") var sectionName: String?) {
    constructor() : this(null, null)
}