package com.CGO.myawesomeCV

import java.util.*
import javax.persistence.*

@Entity(name = "subsections")
data class Subsection(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "title") var title: String?,
        @Column(name = "place") var place: String?,
        @Column(name = "starting_date") var startingDate: Date?,
        @Column(name = "final_date") var finalDate: Date?,
        @Column(name = "summary") var summary: String?) {
    constructor() : this(null, null, null, null, null, null)
}