create table users
(
    id bigint auto_increment,
    constraint users_pk
        primary key (id),
    first_name VARCHAR(100) not null,
    last_name VARCHAR(100) not null,
    age int not null,
    address VARCHAR(255) not null,
    job_status VARCHAR(100) not null,
    mail VARCHAR(255) not null,
    photo VARCHAR(100) not null,
);

create table sections
(
    id bigint auto_increment,
    constraint sections_pk
        primary key (id),
    section_name VARCHAR(100) not null,
);

create table subsections
(
    id bigint auto_increment,
    constraint subsections_pk
        primary key (id),
    title VARCHAR(100) not null,
    place VARCHAR(255) null,
    starting_date date null,
    final_date date null,
    summary TEXT null,
)

create table links
(
    id bigint auto_increment,
    constraint links_pk
        primary key (id),
    social_network TEXT not null,
    link_network TEXT null,
    is_checked TINYINT default 0,
);
